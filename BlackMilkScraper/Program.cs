﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace BlackMilkScraper
{
	class Program
	{
		private const string FILE_NAME_XML = "storage.xml";
		private const string FILE_NAME_PAGES = "pages.txt";
		private static string dataFolder;
	    private static readonly List<Item> Items = new List<Item>();
        static void Main()
		{
			dataFolder = Path.Combine(Path.GetDirectoryName(Directory.GetParent(Directory.GetCurrentDirectory()).FullName), "Data\\");

	        IEnumerable<string> lines = File.ReadLines(dataFolder + FILE_NAME_PAGES);
            foreach (string line in lines)
            {
                GetItems(line);
            }

			Serialise(Items);

			WordFrequency(Items);

			Console.WriteLine("Done");
			Console.ReadLine();
		}

		private static void WordFrequency(List<Item> itemsToAnalyse)
		{
			List<Word> words = new List<Word>();

			foreach (Item item in itemsToAnalyse)
				foreach (string word in item.Name.Split(' '))
					if (words.Exists(x => x.WordItem == word))
						words.First(x => x.WordItem == word).Count++;
					else
						words.Add(new Word {WordItem = word, Count = 1});

			words.Sort((x, y) => x.Count.CompareTo(y.Count));

			TextWriter tw = new StreamWriter("words.txt");

			foreach (var word in words)
				tw.WriteLine($"{word.WordItem}: {word.Count}");

			tw.Close();
		}

		private static void Serialise(List<Item> itemsToSave)
		{
			XmlSerializer xs = new XmlSerializer(typeof (List<Item>));
			using (FileStream fs = new FileStream(Path.Combine(dataFolder,FILE_NAME_XML), FileMode.Create))
			{
				xs.Serialize(fs, itemsToSave);
			}
		}

		private static void GetItems(string url)
		{
			Console.WriteLine($"Processing page {url}");
			bool productsOnPage = true; //check if we're on a valid page and not a 404
			int pageNumber = 1;

			while (productsOnPage)
			{
				Console.WriteLine($"Getting page number {pageNumber}");
				string html;
				try
				{
					html = GetHtml($"{url}?page={pageNumber}");
				}
				catch (WebException)
				{
				    return;
				}

				if (Regex.IsMatch(html, "Sorry, there are no products in this collection"))
				{
					productsOnPage = false;
					Console.WriteLine($"Finished on page {pageNumber}");
				}

				var wholeItem = Regex.Matches(html, "<div class=\"grid__item large--one-(?:third|sixth) medium--one-(?:half|third)(?: small--one-half )? (?: sold-out)?(?: first| last|)\">(.|\n)+?\\s+</div>"); //this regex returns a section of the html where every sectioon describes a single item

				foreach (Match match in wholeItem)
				{
					List<string> images = (from Match match2 in Regex.Matches(match.ToString(), "<source srcset=\"(//cdn.shopify.com.+?1024x1024.+?)\"") select match2.Groups[1].ToString()).ToList(); //get the 2 images linked to the item
					Match nameAndUrl = Regex.Match(match.ToString(), "<a href=\"(.+?)\">(.+?)</a>");
					string itemUrl = nameAndUrl.Groups[1].ToString();
					string itemName = nameAndUrl.Groups[2].ToString();
				    string collection = Regex.Match(url, "/([a-z-]+)$").Groups[1].ToString();
                    collection = char.ToUpper(collection[0]) + collection.Substring(1);
                    if (collection != "Museum")////museum pages are different in format, e.g. don't contain price and availability info
                    {
						Match priceMatch = Regex.Match(match.ToString(), "<span class='money'>\\$([0-9]+\\.[0-9]+) ([A-Z]{3})");
						double price = double.Parse(priceMatch.Groups[1].ToString());
						string currency = priceMatch.Groups[2].ToString();

						Console.Write($"Item {itemName}, ${price}{currency} Available in: ");

						List<Availability> availabilities = new List<Availability>();
						string availability = Regex.Match(match.ToString(), "<div class=\"product-size-options-and-stock\">(.|\\n)+?</div>").ToString();
						foreach (Match match2 in Regex.Matches(availability, "<span( class=\"lowstock\")?>([A-Z ]+)</span>"))
						{
							string size = match2.Groups[2].ToString();
							bool lowStock = !string.IsNullOrEmpty(match2.Groups[1].ToString());

							availabilities.Add(new Availability {Size = (Size)Enum.Parse(typeof(Size), size.Replace(" ", "")) , LowStock = lowStock});

							Console.Write($"{size} ");
							if (lowStock)
								Console.Write("(L) ");
						}
                        Item itemExists = Items.FirstOrDefault(x => x.Name == itemName);
                        if (itemExists != null)
                            itemExists.Collections.Add(collection);
                        else
						    Items.Add(new Item { ImageUrls = images, Price = price, Currency = currency, Name = itemName, Url = itemUrl, Availabilities = availabilities, Collections = new List<string> {collection} });
					}
					else
					{
                        Item itemExists = Items.FirstOrDefault(x => x.Name == itemName);
                        if (itemExists != null)
                            itemExists.Collections.Add(collection);
                        else
                            Items.Add(new Item {ImageUrls = images, Name = itemName, Url = itemUrl, Collections = new List<string> { collection } });
					}
				    Console.WriteLine();
                    //Thread.Sleep(100);
				}

				Console.WriteLine($"Found {wholeItem.Count} items");
				pageNumber++;
			}
		}

		public static string GetHtml(string url)
		{
			HttpWebRequest imageGetRequest = (HttpWebRequest)WebRequest.Create(url);
			imageGetRequest.Method = WebRequestMethods.Http.Get;
			imageGetRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
			imageGetRequest.AllowWriteStreamBuffering = true;
			imageGetRequest.ProtocolVersion = HttpVersion.Version11;
			imageGetRequest.AllowAutoRedirect = true;
			imageGetRequest.ContentType = "application/x-www-form-urlencoded";

			HttpWebResponse imageGetResponse = (HttpWebResponse)imageGetRequest.GetResponse();

			using (StreamReader sr = new StreamReader(imageGetResponse.GetResponseStream()))
			{
				return sr.ReadToEnd();
			}
		}
	}

	public class Item
	{
		public string Name { get; set; }
		public string Url { get; set; }
		public List<string> ImageUrls { get; set; }
		public double Price { get; set; }
		public string Currency { get; set; }
		public List<Availability> Availabilities { get; set; }
		public List<string> Collections { get; set; }
	}

	public class Availability
	{
		public Size Size { get; set; }
		public bool LowStock { get; set; }
	}

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum Size
	{
		XXS,
		XS,
		S,
		M,
		L,
		XL,
        LTXXS,
        LTXS,
        LTS,
        LTM,
        LTL,
        LTXL,
        OSFA,
        OSFM
	}

	public class Word
	{
		public string WordItem { get; set; }
		public int Count { get; set; }
	}
}
